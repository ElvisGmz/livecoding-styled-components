import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {createGlobalStyle} from 'styled-components'
import reportWebVitals from './reportWebVitals';

const Globales = createGlobalStyle`
  body{
    min-height: 100vh;
    background-color: #21212C;
    margin: 0;
    color: #FFF;
    font-family: Verdana, Geneva, Tahoma, sans-serif;
  }
`

ReactDOM.render(
  <React.StrictMode>
    <Globales />
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
