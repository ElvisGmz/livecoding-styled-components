import React, {useState} from "react";
import styled from "styled-components"


export default function Navbar() {

const [navOpen, setNavOpen] = useState(false);

  return (
    <React.Fragment>
      <Container>
        <Nav>
          <NavContainer>
            <Logo>Logo</Logo>
            <LinksContainer open={navOpen ? '0' : '100%'}>
              <a href="">Item 1</a>
              <a href="">Item 2</a>
              <a href="">Item 3</a>
              <a href="">Item 4</a>
              <a href="">Item 5</a>
            </LinksContainer>
            <BarButton onClick={()=>setNavOpen(!navOpen)}>
                <div></div>
                <div></div>
                <div></div>
            </BarButton>
          </NavContainer>
        </Nav>
      </Container>
    </React.Fragment>
  );
}


const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
`;

const Nav = styled.header`
  background-color: crimson;
  padding: 10px;
`;
const NavContainer = styled.div`
  max-width: 1144px;
  margin: auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Logo = styled.span`
  font-size: 22px;
  text-transform: uppercase;
`;

const LinksContainer = styled.div`
    display: flex;

  a {
    color: #fff;
    text-decoration: none;
    padding: 14px;
    transition: all 200ms;

    &:hover {
      background-color: dodgerblue;
    }
  }

  @media only screen and (max-width: 580px){
        position: absolute;
        width: 100vw;
        height: calc(100vh - 55px);
        top: 55px;
        left: ${({open}) => open};
        flex-flow: column nowrap;
        justify-content: center;
        background-color: crimson;
        transition: all 200ms;

        a{
            width: 100%;
            box-sizing: border-box;
            text-align: center;
        }
    }

`;

const BarButton = styled.span`
    width: 35px;
    height: 35px;
    box-sizing: border-box;
    padding: 5px;
    display: flex;
    flex-flow: column wrap;
    justify-content: space-evenly;
    cursor: pointer;
    display: none;

    &:hover{
        background-color: dodgerblue;
    }

    @media only screen and (max-width: 580px){
        display: flex;

        
    &:hover {
      background-color: transparent;
    }
    }

    div{
        background-color: #FFF;
        height: 3px;
        width: 100%;
    }
`