import React from "react";
import styled from "styled-components";

const Container = styled.div`
  max-width: 1144px;
  margin: auto;

  h1 {
    text-align: center;
  }
`;

const Cards = styled.div`
  display: flex;
  justify-content: center;
  flex-flow: row wrap;
`;

const Card = styled.div`
  background-color: #fff;
  color: #000;
  box-sizing: border-box;
  flex: 30%;
  min-width: 250px;
  max-width: 371.33px;
  border-radius: 4px;
  margin: 5px;
  overflow: hidden;

  div {
    padding: 5px 20px;

    h2 {
      color: purple;
    }

    p {
      font-size: 14px;
    }
  }

  img {
    width: 100%;
  }
`;

const Hero = styled.div`
  img {
    width: 100%;
    max-height: 700px;
    object-fit: cover;
  }
`;

export default function Home() {
  return (
    <React.Fragment>
      <Hero>
        <img
          src="https://adventistascastelar.files.wordpress.com/2012/01/amanecer.jpeg"
          alt=""
        />
      </Hero>
      <Container>
        <h1>Hola Mundo</h1>
        <Cards>
          <Card>
            <img
              src="https://lh3.googleusercontent.com/pN5VyD2bfu1DdkemQQ4lGvfj6r8rFEDhyzOOfkv89xhnR-7_hWo7WUl-Av5kOpuRcYGXMYdGrbCeZeEnbU-H4669fg=w640-h400-e365-rj-sc0x00ffffff"
              alt=""
            />
            <div>
              <h2>Hola</h2>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Eligendi, officiis fuga maxime ex natus repudiandae
                perspiciatis, alias ipsum quae eveniet necessitatibus sequi
                similique architecto suscipit voluptatem vitae autem nobis
                expedita?
              </p>
            </div>
          </Card>
          <Card>
            <img
              src="https://lh3.googleusercontent.com/pN5VyD2bfu1DdkemQQ4lGvfj6r8rFEDhyzOOfkv89xhnR-7_hWo7WUl-Av5kOpuRcYGXMYdGrbCeZeEnbU-H4669fg=w640-h400-e365-rj-sc0x00ffffff"
              alt=""
            />
            <div>
              <h2>Hola</h2>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Eligendi, officiis fuga maxime ex natus repudiandae
                perspiciatis, alias ipsum quae eveniet necessitatibus sequi
                similique architecto suscipit voluptatem vitae autem nobis
                expedita?
              </p>
            </div>
          </Card>
          <Card>
            <img
              src="https://lh3.googleusercontent.com/pN5VyD2bfu1DdkemQQ4lGvfj6r8rFEDhyzOOfkv89xhnR-7_hWo7WUl-Av5kOpuRcYGXMYdGrbCeZeEnbU-H4669fg=w640-h400-e365-rj-sc0x00ffffff"
              alt=""
            />
            <div>
              <h2>Hola</h2>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Eligendi, officiis fuga maxime ex natus repudiandae
                perspiciatis, alias ipsum quae eveniet necessitatibus sequi
                similique architecto suscipit voluptatem vitae autem nobis
                expedita?
              </p>
            </div>
          </Card>
          <Card>
            <img
              src="https://lh3.googleusercontent.com/pN5VyD2bfu1DdkemQQ4lGvfj6r8rFEDhyzOOfkv89xhnR-7_hWo7WUl-Av5kOpuRcYGXMYdGrbCeZeEnbU-H4669fg=w640-h400-e365-rj-sc0x00ffffff"
              alt=""
            />
            <div>
              <h2>Hola</h2>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Eligendi, officiis fuga maxime ex natus repudiandae
                perspiciatis, alias ipsum quae eveniet necessitatibus sequi
                similique architecto suscipit voluptatem vitae autem nobis
                expedita?
              </p>
            </div>
          </Card>
          <Card>
            <img
              src="https://lh3.googleusercontent.com/pN5VyD2bfu1DdkemQQ4lGvfj6r8rFEDhyzOOfkv89xhnR-7_hWo7WUl-Av5kOpuRcYGXMYdGrbCeZeEnbU-H4669fg=w640-h400-e365-rj-sc0x00ffffff"
              alt=""
            />
            <div>
              <h2>Hola</h2>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Eligendi, officiis fuga maxime ex natus repudiandae
                perspiciatis, alias ipsum quae eveniet necessitatibus sequi
                similique architecto suscipit voluptatem vitae autem nobis
                expedita?
              </p>
            </div>
          </Card>
          <Card>
            <img
              src="https://lh3.googleusercontent.com/pN5VyD2bfu1DdkemQQ4lGvfj6r8rFEDhyzOOfkv89xhnR-7_hWo7WUl-Av5kOpuRcYGXMYdGrbCeZeEnbU-H4669fg=w640-h400-e365-rj-sc0x00ffffff"
              alt=""
            />
            <div>
              <h2>Hola</h2>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Eligendi, officiis fuga maxime ex natus repudiandae
                perspiciatis, alias ipsum quae eveniet necessitatibus sequi
                similique architecto suscipit voluptatem vitae autem nobis
                expedita?
              </p>
            </div>
          </Card>
          <Card>
            <img
              src="https://lh3.googleusercontent.com/pN5VyD2bfu1DdkemQQ4lGvfj6r8rFEDhyzOOfkv89xhnR-7_hWo7WUl-Av5kOpuRcYGXMYdGrbCeZeEnbU-H4669fg=w640-h400-e365-rj-sc0x00ffffff"
              alt=""
            />
            <div>
              <h2>Hola</h2>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Eligendi, officiis fuga maxime ex natus repudiandae
                perspiciatis, alias ipsum quae eveniet necessitatibus sequi
                similique architecto suscipit voluptatem vitae autem nobis
                expedita?
              </p>
            </div>
          </Card>
        </Cards>
      </Container>
    </React.Fragment>
  );
}
